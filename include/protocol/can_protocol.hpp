/*
 * can_protocol.hpp
 *
 *  Created on: 20/04/2023
 *      Author: Frolov
 */

#ifndef INCLUDE_CAN_RADAR_PROTOCOL_H_
#define INCLUDE_CAN_RADAR_PROTOCOL_H_

#include <stdint.h>

/* Data field length. Use 1, 2, 4, 6 or 8 values only.
 * The data structures should be multiple of CAN_DLC_4
 * */
#define	CAN_PROTOCOL_VERSION		(0x1U)
#define CAN_PCL_MSG_ID				(0xFEU)

#define CAN_DLC						(8U)
#define CAN_DLC_4					(4U)

#define CAN_MAX_POINTS_NUM			(256U)

#define CAN_PREHEADER_MSG_ID_OFFSET	(0x100U)
#define CAN_HEADER_MSG_ID_OFFSET	(0x101U)
#define CAN_PAYLOAD_MSG_ID_OFFSET	(0x110U)

#define CAN_ENABLE_MSG_PENDING		(1U)

#define CAN_VELOCITY_SCALE			(100.0F)
#define CAN_RANGE_SCALE				(100.0F)
#define CAN_ANGLE_SCALE				(100.0F)

typedef struct {
	uint8_t 		version;
	uint8_t 		msg_id;
	uint16_t 		length;
} canPreHeader_t;

typedef struct {
	uint32_t 		frame_cnt;
	uint32_t 		timestamp_low;
	uint32_t 		reserved0;
	uint16_t 		flags;
	uint16_t 		pnts_num;
	int16_t 		ego_velocity;
	uint16_t 		reserved1;
	uint8_t 		noise_level;
	uint8_t 		reserved2;
	uint16_t 		crc16;
} canHeader_t;

typedef struct {
	uint16_t		range;
	int16_t			velocity;
	int16_t			azimuth;
	int16_t			elevation;
	uint8_t			snr;
	int8_t			rcs;
	uint16_t		reserved0;
} canPoint_t;

typedef struct {
	canPreHeader_t 	pre_header;
	canHeader_t 	header;
	canPoint_t 		point[CAN_MAX_POINTS_NUM];
} canDatagram_t;

int32_t canDatagramTx(canDatagram_t* datagram);

#endif /* INCLUDE_CAN_RADAR_PROTOCOL_H_ */
