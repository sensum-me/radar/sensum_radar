#include <ros/ros.h>
#include <ros/console.h>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <boost/thread.hpp>

#include <memory>
#include <string>
#include <limits>

#include "protocol/can_protocol.hpp"
#include "crc16/crc16-ccitt-algorithm.hpp"
#include "pcl_point_type/radar_pcl.hpp"

enum class RECIVE_STATE :uint16_t
{
  HEADER_WAIT,
  DATA_WAIT
};

class CANReceiver {
 public:
  CANReceiver(ros::NodeHandle &nh, ros::NodeHandle &nhp);
  void start();
 private:
  void publish_pc();
  void data_manager(std::shared_ptr<can_frame>& rec_ptr);
  void data_rec();

 private:
  ros::NodeHandle nh_;
  ros::NodeHandle nhp_;

  std::string topic_;
  std::string frame_id_;
  std::string interface_name_;
  bool check_crc16_;

  ros::Publisher radar_pub_static_;
  ros::Publisher radar_pub_dynamic_;
  ros::Time time_stamp_;

  RECIVE_STATE state_;
  struct sockaddr_can addr_;
  struct ifreq ifr_;
  int natsock_;

  boost::asio::io_service ios_;
  boost::asio::posix::stream_descriptor stream_;
  
  canDatagram_t CS_{};

  boost::thread_group workers_;
};
