#include <ros/ros.h>
#include <ros/console.h>
#include <iostream>
#include <math.h>

#include <thread>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <string>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>

#include "crc16/crc16-ccitt-algorithm.hpp"
#include "pcl_point_type/radar_pcl.hpp"

#define UDP_PROTOCOL_VERSION (1U)
#define UDP_MSG_ID_PCL (0xFE)

#define UDP_PNTS_MAX   (32U)
#define EXPECTED_MAX_AMOUNT_OF_POINTS_IN_CLOUD (512U)

#define PI_BY_180 (M_PI / 180.0)

// Preheader
typedef struct {
	uint8_t  version;
	uint8_t  msg_id;
	uint16_t length;
} pre_hdr_t;

// Header
typedef struct {
	uint32_t frame_cnt;
	uint16_t udp_total;
	uint16_t udp_idx;
	uint16_t flags;
	uint16_t udp_pnts_num;
	float    self_velocity;
	float    noise_lvl;
	uint8_t  pcl_type;
	uint8_t  data_type;
	uint8_t  time_type;
	uint8_t  reserved0;
	uint32_t timestamp_low;
	uint32_t timestamp_hight;
	int32_t  reserved1;
	int16_t  reserved2;
	uint16_t crc16;
} hdr_pcl_t;

// Single point
typedef struct {
	float range;
	float velocity;
	float azm;
	float elv;
	float snr;
	float rcs;
	int32_t reserved0;
} pcl_data_t;

// UDP-packet structure
typedef struct {
	pre_hdr_t  pre_header;
  hdr_pcl_t  pcl_header;
  pcl_data_t payload[UDP_PNTS_MAX];
} msg_pcl_t;

class UdpReceiver
{
private:
  ros::NodeHandle nh_;
  ros::NodeHandle nhp_;
  ros::Time time_stamp_;

  unsigned char buf_[sizeof(msg_pcl_t)];
  size_t msg_pcl_t_size_ = sizeof(msg_pcl_t);

  uint32_t points_received_{0};
  int64_t current_frame_{-1};
  int64_t current_cloud_{-1};

  int file_descriptor_{0};

  // full received datagram
  msg_pcl_t dg_;

  // ROS params
  std::string host_ip_;
  std::string multicast_ip_;
  int receive_port_;
  std::string topic_;
  std::string frame_id_;
  std::string ros_debug_enable_;
  bool check_crc16_;
  bool ros_output_{false};

  std::vector<pcl_data_t> points_in_frame_;

  // Publishers
  ros::Publisher radar_pub_static_;
  ros::Publisher radar_pub_dynamic_;

  void receiver();
  bool is_datagramm_valid();
  void publishPointsCloud(int64_t cloud_num);
  void pointCloudInit(pcl::PointCloud<pcl::PointXYZVMT> &);

public:
  UdpReceiver(ros::NodeHandle &nh, ros::NodeHandle &nhp);
};