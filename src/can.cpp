#include "sensum_radar/can.hpp"

CANReceiver::CANReceiver(ros::NodeHandle &nh, ros::NodeHandle &nhp)
    : nh_(nh), nhp_(nhp), stream_(ios_)
{
  nhp_.param("topic", topic_, std::string("can"));
  nhp_.param("frame_id", frame_id_, std::string("radar"));
  nhp_.param("interface_name", interface_name_, std::string("can0"));
  nhp_.param("check_crc16", check_crc16_, false);

  ROS_INFO("topic: %s, frame_id: %s, interface_name: %s",
           topic_.c_str(),
           frame_id_.c_str(),
           interface_name_.c_str());

  // Publishers
  radar_pub_static_  = nh_.advertise<sensor_msgs::PointCloud2>(topic_ + "/static", 10);
  radar_pub_dynamic_ = nh_.advertise<sensor_msgs::PointCloud2>(topic_ + "/dynamic", 10);

  natsock_ = socket(PF_CAN, SOCK_RAW, CAN_RAW);

  strcpy(ifr_.ifr_name, interface_name_.c_str());
  ioctl(natsock_, SIOCGIFINDEX, &ifr_);

  addr_.can_family = AF_CAN;
  addr_.can_ifindex = ifr_.ifr_ifindex;
  if (bind(natsock_, (struct sockaddr*)&addr_, sizeof(addr_)) < 0) {
    perror("Error in socket bind");
    return;
  }

  stream_.assign(natsock_);
}

void CANReceiver::start() {
  // post first task
  ios_.post(boost::bind(&CANReceiver::data_rec, this));

  // set up the worker threads in a thread group
  for (int i = 0; i < 2; ++i) {
    std::cout << std::endl;
    workers_.create_thread([this]() {
      this->ios_.run();
    });
  }

  workers_.join_all();
}


void CANReceiver::publish_pc() {
  pcl::PointCloud<pcl::PointXYZVMT> cloud;
  sensor_msgs::PointCloud2 msg{};

  if ( CS_.pre_header.length == sizeof(canHeader_t) ) {
    cloud.reserve(0);
    pcl::toROSMsg(cloud, msg);
    msg.header.frame_id = frame_id_;
    msg.header.stamp = time_stamp_;

    if (CS_.header.flags == 0) {
      radar_pub_static_.publish(msg);
    } else {
      radar_pub_dynamic_.publish(msg);
    }

    CS_ = {};
    return;
  }

  double x, y, z, self_v;
  double azm, elv, range, v;
  canPoint_t pnt;

  uint32_t pc_size = (CS_.pre_header.length - sizeof(canHeader_t)) / sizeof(canPoint_t);
  if ( (CS_.pre_header.length - sizeof(canHeader_t)) % sizeof(canPoint_t) != 0 ) ++pc_size;

  cloud.reserve(pc_size);

  for (uint32_t i = 0; i < pc_size; ++i) {
    pnt = CS_.point[i];

    azm = pnt.azimuth * 0.001;
    elv = pnt.elevation * 0.001;
    range = pnt.range * 0.01;
    v = pnt.velocity * 0.01;

    x = range * cos(azm) * cos(elv);
    y = -range * sin(azm) * cos(elv);
    z = range * sin(elv);
    self_v = CS_.header.ego_velocity * 0.01;

    cloud.push_back(pcl::PointXYZVMT {
      {{static_cast<float>(std::round(x*1000) / 1000),
        static_cast<float>(std::round(y*1000) / 1000),
        static_cast<float>(std::round(z*1000) / 1000), 1.0f}},
        static_cast<float>(std::round(v*100) / 100),
        static_cast<float>(std::round(self_v*100) / 100),
        static_cast<float>(std::round(pnt.snr*10) / 10),
        static_cast<float>(std::round(pnt.rcs*10) / 10),
        static_cast<uint32_t>(time_stamp_.sec),
        static_cast<uint32_t>(time_stamp_.nsec),
        static_cast<uint32_t>(0)
      });
  }

  pcl::toROSMsg(cloud, msg);
  msg.header.frame_id = frame_id_;
  msg.header.stamp = time_stamp_;

  // publish message
  if (CS_.header.flags == 0) {
    radar_pub_static_.publish(msg);
  } else {
    radar_pub_dynamic_.publish(msg);
  }

  // ROS_INFO("PC published");
  CS_ = {};
}


void CANReceiver::data_manager(std::shared_ptr<can_frame>& rec_ptr) {

  static constexpr uint32_t max_dg_num = 0x110 + (CAN_MAX_POINTS_NUM*sizeof(canPoint_t) / sizeof(can_frame::data));
  static uint32_t last_dg_id{0};
  static uint8_t header_load{0};

  if ( rec_ptr->can_id < 0x100 || rec_ptr->can_id > max_dg_num ) {
    ROS_WARN_STREAM("Wrong can_id: 0x" << std::hex << rec_ptr->can_id << std::dec);
    return;
  }

  // ROS_INFO_STREAM("0x" << std::hex << rec_ptr->can_id << std::dec);

  switch ( uint8_t shift; rec_ptr->can_id )
  {
  case 0x100:
    if ( state_ != RECIVE_STATE::HEADER_WAIT )
      publish_pc();
    time_stamp_ = ros::Time::now();
    state_ = RECIVE_STATE::HEADER_WAIT;
    header_load = 0b1;
    memcpy((uint8_t*)&CS_.pre_header, rec_ptr->data, sizeof(canPreHeader_t));
    // ROS_INFO_STREAM(CS_.header.frame_cnt << " " << std::bitset<8>{header_load});
    break;

  case 0x101:
  case 0x102:
  case 0x103:
    state_ = RECIVE_STATE::HEADER_WAIT;
    shift = (rec_ptr->can_id - 0x101) * 8;           // 0 8 16
    header_load |= 0b1 << (rec_ptr->can_id - 0x100); // 0b10 0b100 0b1000
    memcpy((uint8_t*)&CS_.header + shift, rec_ptr->data, rec_ptr->can_dlc);
    // ROS_INFO_STREAM(CS_.header.frame_cnt << " " << std::bitset<8>{header_load});
    if (header_load == 0b1111) {
      state_ = RECIVE_STATE::DATA_WAIT;
      header_load = 0b0;
      last_dg_id = (0x110 - 1) + (CS_.pre_header.length - sizeof(canHeader_t)) / sizeof(can_frame::data);
      if ( CS_.pre_header.length % sizeof(can_frame::data) != 0 ) ++last_dg_id;
      // ROS_INFO_STREAM("last_dg_id " << std::hex << last_dg_id << std::dec << std::endl);
    }
    break;

  default:
    if ( state_ == RECIVE_STATE::HEADER_WAIT )
      return;
  }

  if ( state_ == RECIVE_STATE::DATA_WAIT ) {
    if ( rec_ptr->can_id < 0x110 || rec_ptr->can_id > last_dg_id ) return;
    uint8_t idx = rec_ptr->can_id - 0x110;
    memcpy((uint8_t*)&CS_.point + idx*sizeof(can_frame::data), rec_ptr->data, rec_ptr->can_dlc);

    if ( rec_ptr->can_id == last_dg_id ) {
      publish_pc();
      header_load = 0b0;
      last_dg_id = std::numeric_limits<uint32_t>::max();
      state_ = RECIVE_STATE::HEADER_WAIT;
    }
  }
}


void CANReceiver::data_rec() {
  while ( ros::ok() ) {
    auto rec_ptr = std::make_shared<can_frame>();
    stream_.read_some(boost::asio::buffer(rec_ptr.get(), sizeof(can_frame)));

    ios_.post([this, u = std::move(rec_ptr)]() mutable {
      this->data_manager(u);
    });
  }
}
