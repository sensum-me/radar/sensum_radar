#include "sensum_radar/driver.hpp"

#define ROSCONSOLE_MIN_SEVERITY ROSCONSOLE_SEVERITY_DEBUG

UdpReceiver::UdpReceiver(ros::NodeHandle &nh, ros::NodeHandle &nhp): nh_(nh), nhp_(nhp) {
  // Read ros parameters
  nhp_.getParam("host_ip", host_ip_);
  nhp_.getParam("multicast_ip", multicast_ip_);
  nhp_.getParam("receive_port", receive_port_);
  nhp_.getParam("topic", topic_);
  nhp_.getParam("frame_id", frame_id_);
  nhp_.param("check_crc16", check_crc16_, true);
  nhp_.param("ros_debug_enable", ros_debug_enable_, std::string("false"));  // "true", "false" or "log"

  if (ros_debug_enable_ == std::string("true")) {
    ros_output_ = true;
  }
  else if (ros_debug_enable_ == std::string("false")) {
    ros_output_ = false;
  }
  else if (ros_debug_enable_ == std::string("log")) {
    ros_output_ = true;
    if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
      ros::console::notifyLoggerLevelsChanged();
    }
  }

  ROS_INFO("host_ip: %s, multicast_ip: %s, receive_port: %d",
           host_ip_.c_str(),
           multicast_ip_.c_str(),
           receive_port_);
  ROS_INFO("pc2 topic: %s frame_id: %s",
           topic_.c_str(),
           frame_id_.c_str());

  // Publishers
  radar_pub_static_  = nh_.advertise<sensor_msgs::PointCloud2>(topic_ + "/static", 10);
  radar_pub_dynamic_ = nh_.advertise<sensor_msgs::PointCloud2>(topic_ + "/dynamic", 10);


  // Set socket
  file_descriptor_ = socket(AF_INET, SOCK_DGRAM, 0);
  if (file_descriptor_ < 0) {
    ROS_ERROR("Socket failure");
  }
  u_int yes = 1;
  if (
    setsockopt(
      file_descriptor_, SOL_SOCKET, SO_REUSEADDR, (char*) &yes, sizeof(yes)
    ) < 0
  ){
    ROS_ERROR("Reusing ADDR failed");
  }

  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY); // differs from sender
  addr.sin_port = htons(receive_port_);

  if (bind(file_descriptor_, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
    ROS_ERROR("Bind failure");
  }

  struct ip_mreq mreq;
  mreq.imr_interface.s_addr = inet_addr(host_ip_.c_str());
  mreq.imr_multiaddr.s_addr = inet_addr(multicast_ip_.c_str());

  while(ros::ok()) {
    if (
      setsockopt(
        file_descriptor_, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*) &mreq, sizeof(mreq)
      ) < 0
    ){
      ROS_ERROR("setsockopt failure, try to reconnect..");
      ros::Duration(0.5).sleep();
    }
    else {
      break;
    }
  }

  memset(&buf_, 0, sizeof(unsigned char)*sizeof(msg_pcl_t));
  points_in_frame_.reserve(EXPECTED_MAX_AMOUNT_OF_POINTS_IN_CLOUD);

  std::thread thread(&UdpReceiver::receiver, this);
  thread.detach();
}


bool UdpReceiver::is_datagramm_valid() {
  if (dg_.pre_header.version != UDP_PROTOCOL_VERSION) {
    ROS_ERROR("Wrong protocol version, expected: %d, received: %d",
              UDP_PROTOCOL_VERSION,
              dg_.pre_header.version);
    return 0;
  }

  if (dg_.pre_header.msg_id != UDP_MSG_ID_PCL) {
    ROS_ERROR("Wrong msg type");
    return 0;
  }

  if (dg_.pcl_header.udp_pnts_num == 0 && dg_.pcl_header.crc16 == 0)
    return 1;

  uint16_t crc16 = CRC16Get(reinterpret_cast<void*>(dg_.payload),
                            dg_.pcl_header.udp_pnts_num*sizeof(pcl_data_t));
  ROS_DEBUG_COND(ros_output_, "crc16: %d, crc16 from radar: %d", crc16, dg_.pcl_header.crc16);

  if (check_crc16_ && crc16 != dg_.pcl_header.crc16) {
    ROS_ERROR("Wrong crc16");
    ROS_ERROR("crc16: %d  dg_.pcl_header.crc16: %d", crc16, dg_.pcl_header.crc16);
    return 0;
  }

  ROS_DEBUG_COND(ros_output_, "frame_cnt: %d", dg_.pcl_header.frame_cnt);
  ROS_DEBUG_COND(ros_output_, "udp_total: %d udp_idx: %d",
                               dg_.pcl_header.udp_total,
                               dg_.pcl_header.udp_idx);
  ROS_DEBUG_COND(ros_output_, "udp_pnts_num: %d", dg_.pcl_header.udp_pnts_num);

  return 1;
}


void UdpReceiver::receiver() {

  while (ros::ok()) {
    ssize_t len_dg = recv(file_descriptor_, buf_, msg_pcl_t_size_, 0);

    if (len_dg < 0) {
      ROS_ERROR("len_dg < 0");
      continue;
    }
    ROS_DEBUG_COND(ros_output_, "buffer len: %ld", static_cast<size_t>(len_dg));

    memset(&dg_, 0, msg_pcl_t_size_);
    memcpy(&dg_, buf_, static_cast<size_t>(len_dg));
    memset(&buf_, 0, msg_pcl_t_size_);

    if (!is_datagramm_valid())
      continue;

    if (dg_.pcl_header.frame_cnt != current_frame_ ||
        dg_.pcl_header.pcl_type != current_cloud_) {
      if (points_received_ != 0)
        publishPointsCloud(current_cloud_);
      time_stamp_ = ros::Time::now();
      points_received_ = 0;
      points_in_frame_.clear();
      current_frame_ = dg_.pcl_header.frame_cnt;
      current_cloud_ = dg_.pcl_header.pcl_type;
    }

    for (size_t i = 0; i < dg_.pcl_header.udp_pnts_num; ++i) {
      points_in_frame_.push_back(dg_.payload[i]);
    }
    points_received_ += dg_.pcl_header.udp_pnts_num;
    if (points_in_frame_.size() > points_received_) {
      points_in_frame_.resize(points_received_);
    }

    if (dg_.pcl_header.udp_idx == dg_.pcl_header.udp_total) {
      publishPointsCloud(current_cloud_);
      points_received_ = 0;
      points_in_frame_.clear();
    }

  }
}


void UdpReceiver::publishPointsCloud(int64_t cloud_num) {
  ROS_DEBUG_COND(ros_output_, "frame assembled");

  //  convert to nano sec and correct to approximate wire transfer time
  uint32_t time_correction = static_cast<uint32_t>(dg_.pcl_header.timestamp_low) * static_cast<uint32_t>(1e3);
  // ROS_WARN("time correction: %d", time_correction);

  ROS_DEBUG_COND(ros_output_, "trgtsNum: %d time_correction %d", points_received_,
                                                                 time_correction);

  if (time_stamp_.nsec >= time_correction) {
    time_stamp_.nsec -= time_correction;
  }
  else {
    time_stamp_.sec--;
    time_stamp_.nsec = (static_cast<uint32_t>(1e9) + time_stamp_.nsec) - time_correction;
  }

  pcl::PointCloud<pcl::PointXYZVMT> cloud;
  cloud.reserve(points_in_frame_.size());

  // PointCloud fields initialization
  pointCloudInit(cloud);

  sensor_msgs::PointCloud2 msg{};
  pcl::toROSMsg(cloud, msg);
  msg.header.frame_id = frame_id_;
  msg.header.stamp = time_stamp_;

  // publish message
  if (cloud_num == 0) {
    radar_pub_static_.publish(msg);
  } else {
    radar_pub_dynamic_.publish(msg);
  }

  ROS_INFO_STREAM_COND(ros_output_, topic_ << " Targets were founded: " << points_received_);
}


void UdpReceiver::pointCloudInit(pcl::PointCloud<pcl::PointXYZVMT> &cloud) {
  double x, y, z, v, self_v;

  for (auto point : points_in_frame_) {
    x = point.range * cos(point.azm) * cos(point.elv);
    y = -point.range * sin(point.azm) * cos(point.elv);
    z = point.range * sin(point.elv);
    v = point.velocity;
    self_v = dg_.pcl_header.self_velocity;

    cloud.push_back(pcl::PointXYZVMT {
      {{static_cast<float>(std::round(x*1000) / 1000),
        static_cast<float>(std::round(y*1000) / 1000),
        static_cast<float>(std::round(z*1000) / 1000), 1.0f}},
      static_cast<float>(std::round(v*100) / 100),
      static_cast<float>(std::round(self_v*100) / 100),
      static_cast<float>(std::round(point.snr*10) / 10),
      static_cast<float>(std::round(point.rcs*10) / 10),
      static_cast<uint32_t>(time_stamp_.sec),
      static_cast<uint32_t>(time_stamp_.nsec),
      static_cast<uint32_t>(0)
      });

  }
}