#include "sensum_radar/driver.hpp"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "driver");

  ros::NodeHandle nh;
  ros::NodeHandle nhp("~");

  UdpReceiver udp_receiver{nh, nhp};

  ros::spin();
  return 0;
}