#include "sensum_radar/can.hpp"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "radar_can");

  ros::NodeHandle nh;
  ros::NodeHandle nhp("~");

  CANReceiver can_receiver{nh, nhp};
  can_receiver.start();

  ros::spin();
  return 0;
}
