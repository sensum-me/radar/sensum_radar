#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>

#include <fstream>
#include <iostream>
#include <pcl_point_type/radar_pcl.hpp>
#include <string>
#include <mutex>

using namespace std;

class RadarPC2Utils {
 private:
  ros::NodeHandle nh_;
  ros::NodeHandle nhp_;

  ros::Subscriber sub_;
  ros::Publisher pub1_;
  ros::Publisher pub2_;

  std::string static_pc_;
  std::string dynamic_pc_;

  size_t iter_{0};
  std::ofstream csv_file_; // protected by mutex_

  std::mutex mutex_;

 public:
  RadarPC2Utils(ros::NodeHandle &nh, ros::NodeHandle &nhp);
  ~RadarPC2Utils();
  void callback(const sensor_msgs::PointCloud2ConstPtr &cloud);
};

RadarPC2Utils::RadarPC2Utils(ros::NodeHandle &nh, ros::NodeHandle &nhp)
    : nh_(nh), nhp_(nhp) {
  nhp_.param("static_pc", static_pc_, std::string("/radar/points/static"));
  nhp_.param("dynamic_pc", dynamic_pc_, std::string("/radar/points/dynamic"));

  csv_file_.open("points.csv");

  sub_ = nh_.subscribe(static_pc_, 100, &RadarPC2Utils::callback, this);
  sub_ = nh_.subscribe(dynamic_pc_, 100, &RadarPC2Utils::callback, this);
}

RadarPC2Utils::~RadarPC2Utils() { 
  csv_file_.close();
}

void RadarPC2Utils::callback(const sensor_msgs::PointCloud2ConstPtr &cloud) {
  pcl::PointCloud<pcl::PointXYZVMT> input_cloud_{};
  pcl::fromROSMsg(*cloud, input_cloud_);

  std::lock_guard<std::mutex> lg(mutex_);

  for (auto point : input_cloud_) {
    csv_file_ << iter_ << "," << point.x << "," << point.y << "," << point.z
              << "," << point.v << "," << point.self_v << "," << point.snr << ","
              << point.rcs << "," << point.sec << "," << point.nsec << "\n";
  }

  ++iter_;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "radar_pc2_utils");
  ros::NodeHandle nh;
  ros::NodeHandle nhp("~");

  RadarPC2Utils pc2_utils{nh, nhp};

  ros::spin();
  return 0;
}