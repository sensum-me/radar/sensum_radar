# Работа без контейнера
## Сборка компонентов
Убедитесь, что в вашем catkin workspace (~/catkin_ws/src) кроме "sensum_radar" есть все нужные компоненты для сборки этого узла: [pcl_point_type](https://gitlab.com/sensum-me/radar/pcl_point_type) и [sensum_tuner](https://gitlab.com/sensum-me/radar/sensum_tuner).

Перейдите в ~/catkin_ws и соберите узел средствами catkin
```bash
cd ~/catkin_ws
source /opt/ros/${ROS_DISTRO}/setup.zsh
catkin_make -DCMAKE_BUILD_TYPE=Release install --only-pkg-with-deps sensum_radar sensum_tuner
```

## Запуск узлов
Перейдите в ~/catkin_ws и запустите launch скрипт (radar.launch или radar_rviz.launch)
```bash
cd ~/catkin_ws
source /opt/ros/${ROS_DISTRO}/setup.zsh
roslaunch sensum_radar radar.launch
```

# Работа с контейнером
## Сборка контейнера
Убедитесь, что в вашем catkin workspace (~/catkin_ws/src) кроме "sensum_radar" есть все нужные компоненты для сборки этого узла: [pcl_point_type](https://gitlab.com/sensum-me/radar/pcl_point_type) и [sensum_tuner](https://gitlab.com/sensum-me/radar/sensum_tuner).

В узле есть два dockerfile'а:
- Dockerfile.noetic - стандартный контейнер;
- Dockerfile.rviz.noetic - контейнер, в котором кроме зависимостей для radar_node также установлен rviz.

Для сборки контейнера выполните следующее:
1. Соберите базовый образ - флаг "-i". При запуске скрипта нужно будет выбрать на основе какого dockerfile'а вы хотите выполнить сборку. Введите 1, если хотите собрать обычный контейнер, или 2, если хотите собрать контейнер с rviz.
```bash
➜ ./build.sh -i
1) Dockerfile.noetic
2) Dockerfile.rviz.noetic
Select dockerfile to build: 
```
2. Соберите контейнер в RELEASE режиме флаг -r. При запуске скрипта также нужно будет выбрать на основе какого dockerfile'а вы хотите выполнить сборку.
```bash
➜ ./build.sh -r
1) Dockerfile.noetic
2) Dockerfile.rviz.noetic
Select dockerfile to build: 
```

## Запуск контейнера
1. Запустите скрипт run_container.sh При запуске скрипта нужно будет выбрать какой образ вы хотите запустить (с rviz или без).
```bash
➜ ./run_container.sh
1) x86_64noetic/radar
2) x86_64noetic/radar_rviz
Select docker image to run:
```

## Подключение к контейнеру
2. Если нужно подключиться к уже запущеному контейнеру, используйте скрипт into.sh. При запуске скрипта нужно будет выбрать какой образ вы хотите запустить (с rviz или без).
```bash
➜ ./into.sh
1) radar
2) radar_rviz
Select docker container to connect:
```
