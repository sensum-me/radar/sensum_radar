#!/bin/bash

yellow=`tput setaf 3`
green=`tput setaf 2`
violet=`tput setaf 5`
reset=`tput sgr0`

release_cmake="catkin_make -DCMAKE_BUILD_TYPE=Release install --only-pkg-with-deps"

path="/home/docker_radar/catkin_ws/src"
tag="release"

dockerfile="Dockerfile.noetic"
image_name=""

main () {
    local str_out="$1 is not a correct key. Choose '-i' ";
    str_out+="for image build or '-r' for release build";

    cd "$(dirname "$0")";
    case "$1" in
        -i) image_build;;
        -r) release_build;;
        *) echo ${yellow}${str_out}${reset};;
    esac
}

dockerfile_selections () {
    PS3="Select dockerfile to build: "
    options=("Dockerfile.noetic" "Dockerfile.rviz.noetic")
    select opt in "${options[@]}"
    do
        case $opt in
            "Dockerfile.noetic")
                dockerfile=$opt
                image_name="sensum4d/node"
                break
                ;;
            "Dockerfile.rviz.noetic")
                dockerfile=$opt
                image_name="sensum4d/radar"
                break
                ;;
            *)  ;; 
        esac
    done
}

image_build () {
    dockerfile_selections;
    echo ${green}"Building image from Dockerfile: ${violet}$dockerfile"${reset};
    docker build . \
        -f ${dockerfile} \
        --tag ${image_name}:base;
}

start_container () {
    echo ${green}"Starting the radar container"${reset};
    docker run -it -d --rm \
        -v `pwd`/../../sensum_radar:${path}/sensum_radar:rw \
        -v `pwd`/../../pcl_point_type:${path}/pcl_point_type:rw \
        -v `pwd`/../../sensum_tuner:${path}/sensum_tuner:rw \
        --net "host" \
        --privileged \
        --name radar ${image_name}:base;
}

release_build () {
    dockerfile_selections;
    docker rmi ${image_name}:release;
    start_container &&
    echo ${green}"Entering the ${image_name} container and building in RELEASE mode: "${reset};
    docker exec --user "docker_radar" -it radar /bin/bash \
                -c ". /opt/ros/noetic/setup.sh; \
                    cd catkin_ws; \
                    ${release_cmake} sensum_radar sensum_tuner; \
                    rm -rf build/ devel/; \
                    exit" \
                    && echo ${green}"Committing the release of radar container: "${reset};
    docker commit radar ${image_name}:release;
    docker stop radar;
}

main "$@"; exit