#!/bin/bash

yellow=`tput setaf 3`
reset_color=`tput sgr0`

path="/home/docker_radar/catkin_ws/src"

running_mode="release"
image_name="sensum4d/radar"
container_name="radar"

main () {
    dockerfile_selections;

    echo -e ${yellow}"\nCheck container.."${reset_color};
    if [ "$(docker container inspect -f '{{.State.Running}}' ${container_name})" = "true" ]; then
        echo -e "Container '${container_name}' is already running!";
        echo -e "connecting..";
        into_container;
    else
        echo -e ${yellow}"\nRun '${container_name}' container.."${reset_color};
        run_container;
    fi
}

dockerfile_selections () {
    PS3="Select docker image to run: "
    options=("sensum4d/node" "sensum4d/radar")
    select opt in "${options[@]}"
    do
        case $opt in
            "sensum4d/node")
                image_name=$opt
                container_name="radar"
                break
                ;;
            "sensum4d/radar")
                echo -e ${yellow}"\nRun 'xhost +'"${reset_color}
                xhost +
                image_name=$opt
                container_name="radar"
                break
                ;;
            *)  ;; 
        esac
    done
}

run_container () {
    if [[ "${image_name}" == "sensum4d/node" ]]; then
        docker run -it -d --rm \
                --net "host" \
                --ipc "host" \
                --pid "host" \
                --privileged \
                --name ${container_name} ${image_name}:release;
        docker exec --user "docker_radar" -it ${container_name} /bin/bash \
                -c "source /opt/ros/noetic/setup.bash;
                    cd catkin_ws;
                    source install/setup.bash;
                    roslaunch sensum_radar radar.launch
                    /bin/bash;";
    fi

    if [[ "${image_name}" == "sensum4d/radar" ]]; then
        sudo docker run -it -d --rm \
            -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
            -e DISPLAY=$DISPLAY \
            -e QT_X11_NO_MITSHM=1 \
            -e XAUTHORITY \
            --net "host" \
            --privileged \
            --name ${container_name} ${image_name}:release

        xhost +local:`docker inspect --format='{{ .Config.Hostname }}' radar`

        sudo docker exec --user "docker_radar" -it radar /bin/bash \
                    -c "source /opt/ros/noetic/setup.sh;
                        cd catkin_ws;
                        source install/setup.bash;
                        roslaunch sensum_radar radar_rviz.launch;
                        /bin/bash;"
    fi
}

into_container () {
    docker exec --user "docker_radar" -it ${container_name} /bin/bash \
            -c "source /opt/ros/noetic/setup.bash;
                cd catkin_ws;
                source install/setup.bash;
                /bin/bash;";
}

main "$@"; exit
