#!/bin/bash

container_name="radar"

main () {
    PS3="Select docker container to connect: "
    options=("radar" "radar_rviz")
    select opt in "${options[@]}"
    do
        case $opt in
            "radar")
                container_name=$opt
                break
                ;;
            "radar_rviz")
                container_name=$opt
                break
                ;;
            *)  ;; 
        esac
    done
    into;
}

into () {
    docker exec --user "docker_radar" -it ${container_name} /bin/bash \
                -c "source /opt/ros/noetic/setup.bash;
                    cd catkin_ws;
                    source install/setup.bash;
                    /bin/bash;"
}

main "$@"; exit
